package PT2017.demo.Project5;
import java.util.Date;

public class MonitoredData
{
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(Date startTime, Date endTime, String activityLabel) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}

	public Date getStartTime() {
		return startTime;
	}
	
	@SuppressWarnings("deprecation")
	public int getStartTimeDate() {
		return (startTime.getYear()+1900)*10000+(startTime.getMonth()+1)*100+startTime.getDate();
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	//durata activitatii in ore
	public double getActivityDuration()
	{
		return (endTime.getTime()-startTime.getTime())*0.001/60/60;
	}
	
	public String toString()
	{
		return startTime + " " + endTime + " " + activityLabel;
	}
}
