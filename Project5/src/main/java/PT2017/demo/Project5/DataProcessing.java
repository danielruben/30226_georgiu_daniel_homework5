package PT2017.demo.Project5;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

public class DataProcessing{
	
	private static List<MonitoredData> monitoredData=new ArrayList<MonitoredData>();
	
	public static void readData()
	{
		monitoredData.clear();
		BufferedReader br;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startTime,endTime;
		try {
			br = new BufferedReader(new FileReader("Activities.txt"));
		    //StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    String act[]=new String[5];

		    while (line != null) {
		       // sb.append(line);
		       // sb.append(System.lineSeparator());
		    	StringTokenizer st = new StringTokenizer(line);
		        for(int i=0;i<5;i++)
		        {
		        	act[i]=st.nextToken();
		        }
		        startTime = formatter.parse(act[0]+" "+act[1]);
		        endTime = formatter.parse(act[2]+" "+act[3]);
		        monitoredData.add(new MonitoredData(startTime,endTime,act[4]));
		        line = br.readLine();
		    }
		    br.close();
		    //String everything = sb.toString();
		    //System.out.println(everything);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	public static void count_days()
	{
		BiFunction<Date,Date,Long> nr_ms=(x,y)->(x.getTime()-y.getTime());
		Function<Long,Integer> ms_to_days=x->(int)(x*0.001/60/60/24)+1;
		System.out.println("Nr zile:"+ms_to_days.apply(nr_ms.apply(monitoredData.get(monitoredData.size()-1).getEndTime(), monitoredData.get(0).getStartTime())));
	}
	
	public static void nr_occurrences_action()
	{
		Map<String,Long> nr_occurrences_action=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		System.out.println(nr_occurrences_action.toString());
		try{
		    PrintWriter writer = new PrintWriter("nr_occurrences_action.txt", "UTF-8");
		    writer.println(nr_occurrences_action.toString());
		    writer.close();
		} 
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		JOptionPane.showMessageDialog(null,"Write successful in nr_occurrences_action.txt");
	}
	
	public static void nr_occurrences_action_per_day()
	{
		Map<Integer, Map<String, Long>>  nr_occurrences_action_per_day=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getStartTimeDate,Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting())));
		System.out.println(nr_occurrences_action_per_day.toString());
		try{
		    PrintWriter writer = new PrintWriter("nr_occurrences_action_per_day.txt", "UTF-8");
		    StringTokenizer st = new StringTokenizer(nr_occurrences_action_per_day.toString(),"}");
		    while (st.hasMoreTokens()) {
		    	writer.println(st.nextToken());
		    }
		    writer.close();
		} 
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		JOptionPane.showMessageDialog(null,"Write successful in nr_occurrences_action_per_day.txt");
	}
	
	public static void activity_duration()
	{
		Map<String, Double> activity_duration=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.summingDouble(MonitoredData::getActivityDuration)));
		System.out.println(activity_duration.toString());
		try{
		    PrintWriter writer = new PrintWriter("activity_duration.txt", "UTF-8");
		    StringTokenizer st = new StringTokenizer(activity_duration.toString());
		    while (st.hasMoreTokens()) {
		    	writer.println(st.nextToken());
		    }
		    writer.close();
		} 
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		JOptionPane.showMessageDialog(null,"Write successful in activity_duration.txt");
	}
	
	public static void main(String args[])
	{
		readData();
		for(MonitoredData md:monitoredData)
		{
			System.out.println(md.toString());
		}
		
		count_days();
		
		nr_occurrences_action();
		
		nr_occurrences_action_per_day();
		
		activity_duration();
		
		//5
		//Map<String,Long> nr_occurrences_action_duration5=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
	}
}

